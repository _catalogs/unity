namespace MyFantasy
{
	/// <summary>
	/// Структура полученных данных - объект
	/// </summary>
	[System.Serializable]
	public class NewObjectRecive : ObjectRecive
	{
		public new NewComponentsRecive components;
	}
}